var express = require("express"),
    router = express.Router(),
    db = require("../models/schema/db");

router.get("/users", function(req, res) {
	try {
	    db.users.find( function(err, userData) {
	    	if (err) {
	    		console.log("Error while finding user:", err);
	    		res.json({
		    	    "err": err
		    	});
	    	} else {
		    	res.json({
		    	    "users": userData
		    	});
			}
	    });
	} catch (e) {
		res.json({
    	    "err": e.stack
    	});	
	}
});

router.get("/users/:user_id", function(req, res) {
	try {
	    db.users.find({_id: req.params.user_id}, function(err, userData) {
	    	if (err) {
	    		console.log("Error while finding user:", err);
	    		res.json({
		    	    "err": err
		    	});
	    	} else{
		    	res.json({
		    	    "users": userData
		    	});
			}
	    });
	} catch (e) {
		res.json({
    	    "err": e.stack
    	});	
	}
});

router.post("/users", function(req, res) {
	try {	
		var reqUser = req.body.user; 
	    
	    db.users.create(reqUser, function(err, userData) {
	    	if (err) {
	    		console.log("Error while inserting new user:", err);
	    		res.json({
		    	    "err": err
		    	});
	    	} else {
		    	res.json({
		    	    "users": userData
		    	});
			}
	    });
	} catch (e) {
		res.json({
    	    "err": e.stack
    	});		
	}
});

router.put("/users/:user_id", function(req, res) {
	try {	
		var reqUserObj = {
			$set: req.body.user
		}; 
	    db.users.findOneAndUpdate({_id: req.params.user_id}, reqUserObj, function(err, userData) {
	    	if (err) {
	    		console.log("Error while updating user:", err);
	    		res.json({
		    	    "err": err
		    	});
	    	} else {
		    	res.json({
		    	    "users": userData
		    	});
			}
	    });
	} catch (e) {
		res.json({
    	    "err": e.stack
    	});		
	}
});

router.delete("/users/:user_id", function(req, res) {
	try {	
	    db.users.remove({_id: req.params.user_id}, function(err, userData) {
	    	if (err) {
	    		console.log("Error while deleting user:", err);
	    		res.json({
		    	    "err": err
		    	});
	    	} else {
		    	res.json({
		    	    "users": userData
		    	});
			}
	    });
	} catch (e) {
		res.json({
    	    "err": e.stack
    	});		
	}
});

router.get("/posts", function(req, res) {
	try {
	    db.posts.find( function(err, postData) {
	    	if (err) {
	    		console.log("Error while finding post:", err);
	    		res.json({
		    	    "err": err
		    	});
	    	} else {
		    	res.json({
		    	    "posts": postData
		    	});
			}
	    });
	} catch (e) {
		res.json({
    	    "err": e.stack
    	});	
	}
});

router.get("/posts/:post_id", function(req, res) {
	try {
	    db.posts.find({_id: req.params.post_id}, function(err, postData) {
	    	if (err) {
	    		console.log("Error while finding post:", err);
	    		res.json({
		    	    "err": err
		    	});
	    	} else{
		    	res.json({
		    	    "posts": postData
		    	});
			}
	    });
	} catch (e) {
		res.json({
    	    "err": e.stack
    	});	
	}
});

router.post("/posts", function(req, res) {
	try {	
		var reqpost = req.body.post; 
	    
	    db.posts.create(reqpost, function(err, postData) {
	    	if (err) {
	    		console.log("Error while inserting new post:", err);
	    		res.json({
		    	    "err": err
		    	});
	    	} else {
		    	res.json({
		    	    "posts": postData
		    	});
			}
	    });
	} catch (e) {
		res.json({
    	    "err": e.stack
    	});		
	}
});

router.put("/posts/:post_id", function(req, res) {
	try {	
		var reqpostObj = {
			$set: req.body.post
		}; 
	    db.posts.findOneAndUpdate({_id: req.params.post_id}, reqpostObj, function(err, postData) {
	    	if (err) {
	    		console.log("Error while updating post:", err);
	    		res.json({
		    	    "err": err
		    	});
	    	} else {
		    	res.json({
		    	    "posts": postData
		    	});
			}
	    });
	} catch (e) {
		res.json({
    	    "err": e.stack
    	});		
	}
});

router.delete("/posts/:post_id", function(req, res) {
	try {	
	    db.posts.remove({_id: req.params.post_id}, function(err, postData) {
	    	if (err) {
	    		console.log("Error while deleting post:", err);
	    		res.json({
		    	    "err": err
		    	});
	    	} else {
		    	res.json({
		    	    "posts": postData
		    	});
			}
	    });
	} catch (e) {
		res.json({
    	    "err": e.stack
    	});		
	}
});

router.get("/categories", function(req, res) {
	try {
	    db.categories.find( function(err, categoryData) {
	    	if (err) {
	    		console.log("Error while finding category:", err);
	    		res.json({
		    	    "err": err
		    	});
	    	} else {
		    	res.json({
		    	    "categories": categoryData
		    	});
			}
	    });
	} catch (e) {
		res.json({
    	    "err": e.stack
    	});	
	}
});

router.get("/categories/:category_id", function(req, res) {
	try {
	    db.categories.find({_id: req.params.category_id}, function(err, categoryData) {
	    	if (err) {
	    		console.log("Error while finding category:", err);
	    		res.json({
		    	    "err": err
		    	});
	    	} else{
		    	res.json({
		    	    "categories": categoryData
		    	});
			}
	    });
	} catch (e) {
		res.json({
    	    "err": e.stack
    	});	
	}
});

router.post("/categories", function(req, res) {
	try {	
		var reqcategory = req.body.category; 
	    
	    db.categories.create(reqcategory, function(err, categoryData) {
	    	if (err) {
	    		console.log("Error while inserting new category:", err);
	    		res.json({
		    	    "err": err
		    	});
	    	} else {
		    	res.json({
		    	    "categories": categoryData
		    	});
			}
	    });
	} catch (e) {
		res.json({
    	    "err": e.stack
    	});		
	}
});

router.put("/categories/:category_id", function(req, res) {
	try {	
		var reqcategoryObj = {
			$set: req.body.category
		}; 
	    db.categories.findOneAndUpdate({_id: req.params.category_id}, reqcategoryObj, function(err, categoryData) {
	    	if (err) {
	    		console.log("Error while updating category:", err);
	    		res.json({
		    	    "err": err
		    	});
	    	} else {
		    	res.json({
		    	    "categories": categoryData
		    	});
			}
	    });
	} catch (e) {
		res.json({
    	    "err": e.stack
    	});		
	}
});

router.delete("/categories/:category_id", function(req, res) {
	try {	
	    db.categories.remove({_id: req.params.category_id}, function(err, categoryData) {
	    	if (err) {
	    		console.log("Error while deleting category:", err);
	    		res.json({
		    	    "err": err
		    	});
	    	} else {
		    	res.json({
		    	    "categories": categoryData
		    	});
			}
	    });
	} catch (e) {
		res.json({
    	    "err": e.stack
    	});		
	}
});

module.exports = router;
