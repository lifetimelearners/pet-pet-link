var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var usersSchema = new Schema({
    username: String,
    password: String,
    email: String,
    firstName: String,
    lastName: String,
    imageUrl: String,
    verificationCode: String,
    isVerified: { type: Boolean, default: false },
    createdon: { type: Date, default: Date.now },
    isActive: { type: Boolean, default: true },
    socialInfo: [{
        userid: String,
        handle: String,
        token: String,
        type: String
    }]
});

var postsSchema = new Schema({
    title: String,
    imageUrl: String,
    content: String,
    username: String,
    userImage: String,
    likeCount: Number,
    flagCount: Number,
    category: [{
        name: String,
        catId: { type: Schema.Types.ObjectId, ref: 'Categories' }
    }],
    userId: { type: Schema.Types.ObjectId, ref: 'Users' },
    likedBy: [{ type: Schema.Types.ObjectId, ref: 'Users' }],
    flaggedBy: [{ type: Schema.Types.ObjectId, ref: 'Users' }],
    isActive: { type: Boolean, default: true },
    createdon: { type: Date, default: Date.now },
    comments: [{
        userId: { type: Schema.Types.ObjectId, ref: 'Users' },
        comment: String,
        createdon: { type: Date, default: Date.now }
    }]
});

var categoriesSchema = new Schema({
    name: String,
    imageUrl: String
});

module.exports.users = mongoose.model('Users', usersSchema);
module.exports.posts = mongoose.model('Posts', postsSchema);
module.exports.categories = mongoose.model('Categories', categoriesSchema);
