var express = require("express"),
    mongoose = require("mongoose"),
    bodyParser = require("body-parser"),
    routes = require("./api/routes");

// Creating express instance
var app = express();
var router = express.Router();

// Making connection with mongo
mongoose.connect("mongodb://localhost:27017/petpetlink");

// parse application/json
app.use(bodyParser.json());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// Setting index file on home page
app.get('/', function(req, res) {
    res.send('Hello world');
});

app.use("/", users);

// Listening server at 4500
app.listen(4500, function() {
    console.log('Express is listening to http://localhost:4500');
})
